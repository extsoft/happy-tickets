package com.dserdiuk.happy.tickets;

import com.dserdiuk.happy.tickets.calculator.HappyTicketCalculator;
import com.dserdiuk.happy.tickets.calculator.MoskowHappyTicketCalculator;
import com.dserdiuk.happy.tickets.calculator.PiterHappyTicketCalculator;
import com.dserdiuk.happy.tickets.calculator.TicketsCalculator;
import com.dserdiuk.happy.tickets.generator.TicketGenerator;
import com.dserdiuk.happy.tickets.input.InputReader;

public class App {

    public static void main(String[] args) {
        String method = new InputReader().getCalculationMethod(args);

        if ("Moskow".equalsIgnoreCase(method)) {
            calculate(new MoskowHappyTicketCalculator());
        } else if ("Piter".equalsIgnoreCase(method)) {
            calculate(new PiterHappyTicketCalculator());
        } else {
            System.out.println("Provided calcluation method is unknown: " + method);
        }
    }

    private static void calculate(HappyTicketCalculator happyTicketCalculator) {
        TicketGenerator ticketGenerator = new TicketGenerator();
        new TicketsCalculator(ticketGenerator, happyTicketCalculator).calculate();
    }
}
