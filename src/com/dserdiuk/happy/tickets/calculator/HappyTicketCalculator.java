package com.dserdiuk.happy.tickets.calculator;

import com.dserdiuk.happy.tickets.Ticket;

/**
 * @author Dmytro Serdiuk
 */
public interface HappyTicketCalculator {

    String methodName();

    boolean isHappy(Ticket ticket);
}
